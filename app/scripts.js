import { tarotCards, tarotSides } from './config.js'

var selectedPokers = []
var totalTarotNum = Array.from({length: 22}, (v, i) => i)
var count = 0

var pokers = document.querySelectorAll('.pokerWrap .poker')

var fivePokersWrapEl = document.querySelector('.fivePokersWrap')
var fivePokersEl = ['.fivePokers--1', '.fivePokers--2', '.fivePokers--3', '.fivePokers--4', '.fivePokers--5']
var viewEl = ['.view--1', '.view--2', '.view--3', '.view--4', '.view--5']
var tarotDescElArr = document.querySelectorAll('.tarotDesc-view')
var startUp = document.querySelector('.btn-start')
var tarotStart = document.querySelector('.tarot-start')
var tarotView = document.querySelector('.tarotView')
var tarotRule = document.querySelector('.tarot-rule')
var tarotInstruction = document.querySelector('.tarot-instruction')
var btnInstruction = document.querySelector('.item--instruction')
var btnPlayAgain = document.querySelector('.tarot-playAgain')



var audio_card_pick = new Howl({
  src: ['audio/card_pick.mp3'],
  volume: 0.5
});
var audio_card_flip = new Howl({
  src: ['audio/card_flip.mp3'],
  volume: 0.5
});
var audio_open_result = new Howl({
  src: ['audio/open_result.mp3'],
  volume: 0.2
});

var audio_bg = new Audio('audio/bg.mp3')
var audio_scale = new Audio('audio/scale.mp3')
var audio_start_hover = new Audio('audio/start_hover.mp3')

// tarotStart.classList.add('is-hide')
// tarotView.classList.add('is-show')
// start()
startUp.addEventListener('click', function() {
  audio_start_hover.play()
  audio_start_hover.volume = 0.5
  tarotStart.classList.add('is-hide')
  tarotView.classList.add('is-show')
  start()
  audio_bg.play()
  audio_bg.volume = 0.5
  audio_bg.loop = true
})

tarotRule.addEventListener('click', function() {
  tarotRule.classList.add('is-hide')
})

btnInstruction.addEventListener('click', function() {
  tarotInstruction.classList.toggle('is-show')
})

tarotInstruction.addEventListener('click', function() {
  tarotInstruction.classList.remove('is-show')
})

btnPlayAgain.addEventListener('click', function() {
  playAgain()
  openFivePokersTransition(false)
  setTimeout(function() {
    openFivePokersTransition(true)
  }, 500)
})

function openFivePokersTransition(isActive) {
  fivePokersEl.forEach(function(fivePokers) {
    if(isActive) {
      document.querySelector(fivePokers).style = null
    } else {
      document.querySelector(fivePokers).style = 'transition: none'
    }
  })
}


function playAgain() {
  audio_start_hover.play()
  audio_start_hover.volume = 0.5
  selectedPokers.length = 0
  totalTarotNum = Array.from({length: 22}, (v, i) => i)
  count = 0

  document.querySelector('.fivePokersWrap').classList.remove('is-scale')
  document.querySelector('.angel').classList.remove('is-show')
  document.querySelector('.pokerWrap').classList.remove('is-hide')

  // 牌橋
  pokers.forEach(function(pokerEl) {
    pokerEl.classList.remove('is-hide')
  })

  // 復原已抽的牌
  fivePokersEl.forEach(function(fivePokers, i) {
    var isContainsHide = document.querySelector(fivePokers).classList.contains('.is-hide')

    if(isContainsHide) {
      return
    }

    document.querySelector(fivePokers).classList.add('is-hide')
    document.querySelector(fivePokers).classList.remove('is-flip')
    document.querySelector(fivePokers).classList.remove('is-highlight')

    document.querySelector(fivePokersEl[i] + ' .fivePokers-img').classList.remove(document.querySelector(fivePokersEl[i] + ' .fivePokers-img').classList.item(1))
    document.querySelector(fivePokersEl[i] + ' .fivePokers-front').classList.remove('is-down')

    document.querySelector(viewEl[i] + ' .fivePokers-img').classList.remove('is-down')

    document.querySelector(viewEl[i] + ' .fivePokers-img').classList.remove(document.querySelector(viewEl[i] + ' .fivePokers-img').classList.item(2))

    document.querySelector(viewEl[i] + ' .tarotDesc-meaning').classList.remove(document.querySelector(viewEl[i] + ' .tarotDesc-meaning').classList.item(1))
    document.querySelector(viewEl[i] + ' .tarotDesc-translation').classList.remove(document.querySelector(viewEl[i] + ' .tarotDesc-translation').classList.item(1))
  })

  // 關閉已開啟的解釋or玩法說明
  tarotDescElArr.forEach(function(tarotDesc) {
    tarotDesc.classList.remove('is-open')
  })
  tarotInstruction.classList.remove('is-show')

  btnPlayAgain.classList.remove('is-show')

}

function start() {
  // close tarot view
  tarotDescElArr.forEach(function(tarotDesc) {
    tarotDesc.addEventListener('click', function() {
      tarotDesc.classList.remove('is-open')
    })
  })

  // flip tarot card by order
  fivePokersEl.forEach(function(fivePokers, i) {
    document.querySelector(viewEl[i] + ' .tarotDesc-span').innerHTML =  '【 解讀 】：'

    // add tarot name
    fivePokersWrapEl.addEventListener('transitionend', function() {
      if(count === i) {
        document.querySelector(fivePokers).classList.add('is-highlight')
      }
    })

    document.querySelector(fivePokers).addEventListener('click', function() {
      if(selectedPokers[i].isOpened) {
        openTarot(i)
      }

      if(count === i) {
        if(isAllFlipped()) {
          document.querySelector(fivePokers).classList.remove('is-highlight')
          selectedPokers[count].isOpened = true
          count += 1

          if(selectedPokers[i].isOpened) {
            openTarot(i)
          }


          if(count > 4) {
            return
          }
          document.querySelector(fivePokersEl[count]).classList.add('is-highlight')

          return
        }

        document.querySelector(fivePokers).classList.add('is-flip')
        document.querySelector(fivePokers).classList.remove('is-highlight')
        selectedPokers[count].isFlipped = true
        count += 1

        if(count > 4) {
          audio_card_flip.play()
          if(isAllFlipped()) {
            count = 0
            document.querySelector(fivePokersEl[count]).classList.add('is-highlight')
          }
          return
        }

        document.querySelector(fivePokersEl[count]).classList.add('is-highlight')
        audio_card_flip.play()
      }
    })
  })

  // pick tarot poker
  pokers.forEach(function(pokerEl, i) {
    pokerEl.addEventListener('click', function() {
      tarotRule.classList.add('is-hide')
      if(hasSelected(i) || selectedPokers.length > 4) {
        return
      }

      audio_card_pick.play()

      getSelectedTarot(i)
      hidePoker(pokerEl)
      showFivePokers(selectedPokers.length)

    })
  })
}

function openTarot(count) {
  tarotDescElArr.forEach(function(tarotDescEl) {
    tarotDescEl.classList.remove('is-open')
  })
  tarotDescElArr[count].classList.add('is-open')
  audio_open_result.play()
  if(count === 4) {
    document.querySelector('.tarot-playAgain').classList.add('is-show')
  }
}

function isAllFlipped() {
  var allFlipped = []
  allFlipped = selectedPokers.filter(function(poker) {
    return poker.isFlipped === false
  })
  return allFlipped.length === 0
}

function getSelectedTarot(idx) {
  var randPos = rand(totalTarotNum.length) // 0 to pokerCodeArr.length
  var randSide = rand(2) // 0 to 1
  var tarotNum = totalTarotNum.splice(randPos, 1)

  selectedPokers.push({
    name: 'name',
    order: String(selectedPokers.length),
    tarotNum: String(tarotNum),
    side: String(randSide),
    isFlipped: false,
    isOpened: false,
    z_randomCode: String(selectedPokers.length) + String(tarotNum) + String(randSide)
  })

  setTarotResult(selectedPokers.length - 1)
  openTarotResult(selectedPokers.length - 1)

  // console.log('selected', selectedPokers)
}


function rand(size) {
   return Math.floor(Math.random() * size)
}

function hasSelected(poker) {
  return selectedPokers.indexOf(poker) !== -1
}

function hidePoker(pokerEl) {
  pokerEl.classList.add('is-hide')
}

function showFivePokers(pokersSizes) {
  document.querySelector('.fivePokers--' + pokersSizes).classList.remove('is-hide')

  if(pokersSizes === 5) {
    document.querySelector('.fivePokersWrap').classList.add('is-scale')
    document.querySelector('.angel').classList.add('is-show')
    document.querySelector('.pokerWrap').classList.add('is-hide')
    var timeout = setTimeout(function() {
      audio_scale.play()
      audio_scale.volume = 0.2
      clearTimeout(timeout)
    }, 1000)
  }
}

function setTarotResult(tarotOrder) {
  // side
  if(selectedPokers[tarotOrder].side === '1') {
    document.querySelector(fivePokersEl[tarotOrder] + ' .fivePokers-front').classList.add('is-down')
  }

  // name
  document.querySelector(fivePokersEl[tarotOrder] + ' .fivePokers-name').innerHTML = tarotCards[selectedPokers[tarotOrder].tarotNum].name

  // roman
  document.querySelector(fivePokersEl[tarotOrder] + ' .fivePokers-roman').innerHTML = tarotCards[selectedPokers[tarotOrder].tarotNum].roman_num

  // tarot img
  document.querySelector(fivePokersEl[tarotOrder] + ' .fivePokers-img').classList.add('img-' + selectedPokers[tarotOrder].tarotNum)
}

function openTarotResult(tarotOrder) {
  if(selectedPokers[tarotOrder].side === '1') {
    document.querySelector(viewEl[tarotOrder] + ' .fivePokers-img').classList.add('is-down')
  }

  document.querySelector(viewEl[tarotOrder] + ' .fivePokers-img').classList.add('img-' + selectedPokers[tarotOrder].tarotNum)
  document.querySelector(viewEl[tarotOrder] + ' .tarotDesc-enName').innerHTML = '(' + tarotCards[selectedPokers[tarotOrder].tarotNum].en_name + ')'
  document.querySelector(viewEl[tarotOrder] + ' .tarotDesc-roman').innerHTML = tarotCards[selectedPokers[tarotOrder].tarotNum].roman_num
  document.querySelector(viewEl[tarotOrder] + ' .tarotDesc-name').innerHTML = tarotCards[selectedPokers[tarotOrder].tarotNum].name
  document.querySelector(viewEl[tarotOrder] + ' .tarotDesc-side').innerHTML = '(' + tarotSides[selectedPokers[tarotOrder].side].side + ')'
  document.querySelector(viewEl[tarotOrder] + ' .tarotDesc-meaning').classList.add('meaning--' + selectedPokers[tarotOrder].tarotNum)
  document.querySelector(viewEl[tarotOrder] + ' .tarotDesc-translation').classList.add('translation--' + selectedPokers[tarotOrder].z_randomCode)
}
