var tarotCards = [
  {
    name: '愚者',
    en_name: 'THE FOOL',
    roman_num: 'O'
  },
  {
    name: '魔術師',
    en_name: 'THE MAGICIAN',
    roman_num: 'I'
  },
  {
    name: '女祭司',
    en_name: 'THE HIGH PREISTESS',
    roman_num: 'II'
  },
  {
    name: '皇后',
    en_name: 'THE EMPRESS',
    roman_num: 'III'
  },
  {
    name: '國王',
    en_name: 'THE EMPEROR',
    roman_num: 'IV'
  },
  {
    name: '教皇',
    en_name: 'THE HIEROHANT',
    roman_num: 'V'
  },
  {
    name: '戀人',
    en_name: 'THE LOVERS',
    roman_num: 'VI'
  },
  {
    name: '戰車',
    en_name: 'THE CHARIOT',
    roman_num: 'VII'
  },
  {
    name: '力量',
    en_name: 'STRENGTH',
    roman_num: 'VIII'
  },
  {
    name: '隱者',
    en_name: 'THE HERMIT',
    roman_num: 'IX'
  },
  {
    name: '命運之輪',
    en_name: 'WHEER OF FORTUNE',
    roman_num: 'X'
  },
  {
    name: '正義',
    en_name: 'JUSTICE',
    roman_num: 'XI'
  },
  {
    name: '倒吊男',
    en_name: 'THE HANGED MAN',
    roman_num: 'XII'
  },
  {
    name: '死神',
    en_name: 'DEATH',
    roman_num: 'XIII'
  },
  {
    name: '節制',
    en_name: 'TEMPERANCE',
    roman_num: 'XIV'
  },
  {
    name: '惡魔',
    en_name: 'THE DEVIL',
    roman_num: 'XV'
  },
  {
    name: '高塔',
    en_name: 'THE TOWER',
    roman_num: 'XVI'
  },
  {
    name: '星星',
    en_name: 'THE STAR',
    roman_num: 'XVII'
  },
  {
    name: '月亮',
    en_name: 'THE MOON',
    roman_num: 'XVIII'
  },
  {
    name: '太陽',
    en_name: 'THE SUN',
    roman_num: 'XIX'
  },
  {
    name: '審判',
    en_name: 'JUDGEMENT',
    roman_num: 'XX'
  },
  {
    name: '世界',
    en_name: 'THE WORLD',
    roman_num: 'XXI'
  },
]

var tarotSides = [
  {
    side: '正位'
  },
  {
    side: '逆位'
  },
]

export { tarotCards, tarotSides }